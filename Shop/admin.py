from django.contrib import admin
from .models import Category, Product, Commentary, Order, OrderItem, ReviewedProducts
from django.utils.safestring import mark_safe
from import_export import resources
from import_export.admin import ImportExportModelAdmin


class ProductResource(resources.ModelResource):
    class Meta:
        model = Product
        prepopulated_fields = {'slug': ('name',)}


class ProductImportAdmin(ImportExportModelAdmin):
    resource_class = ProductResource
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'category', 'get_image', 'price')
    list_display_links = ('name',)
    search_fields = ('name',)
    list_filter = ('name', 'category__name')
    readonly_fields = ('get_image',)

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="120" height="160"')

    get_image.short_description = "Картинка"


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    list_display_links = ('name',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    list_filter = ('name',)


# class ProductAdmin(admin.ModelAdmin):
#     list_display = ('name', 'category', 'get_image', 'price')
#     list_display_links = ('name',)
#     prepopulated_fields = {'slug': ('name',)}
#     search_fields = ('name',)
#     list_filter = ('name', 'category__name')
#     readonly_fields = ('get_image',)
#
#     def get_image(self,obj):
#         return mark_safe(f'<img src={obj.image.url} width="120" height="160"')
#
#     get_image.short_description = "Картинка"


class CommentaryAdmin(admin.ModelAdmin):
    list_display = ('name', 'product', 'user', 'date')
    list_display_links = ('name', 'product')
    search_fields = ('user', 'product')
    list_filter = ('product__name', 'user__username')


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'email', 'address', 'paid', 'created', 'updated']
    list_filter = ['paid', 'created', 'updated']
    inlines = [OrderItemInline]


class ReviewedProductsAdmin(admin.ModelAdmin):
    list_display = ['product', 'date', 'user', 'url']


admin.site.register(Category, CategoryAdmin)
#admin.site.register(Product, ProductAdmin)
admin.site.register(Commentary, CommentaryAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Product, ProductImportAdmin)
admin.site.register(ReviewedProducts, ReviewedProductsAdmin)


