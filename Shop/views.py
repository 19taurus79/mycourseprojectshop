from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import View, ListView, DetailView, CreateView, FormView
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .forms import *
from django.views.decorators.http import require_POST
from .cart import Cart


class ProductView(View):
    def get(self, request, category_slug=None):
        product = Product.objects.all()
        category = None
        categories = Category.objects.all()
        revprod = ReviewedProducts.objects.order_by('-date')[:5]
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            product = product.filter(category=category)

        return render(request, 'product/list.html',
                      {'product_list': product, 'category': category, 'categories': categories,
                       'title': 'Главная страница', 'revprod': revprod})


class ProductDetail(DetailView):
    def get(self, request, id, slug):
        product = get_object_or_404(Product, id=id, slug=slug)
        cart_product_form = CartAddProductForm()
        comments = product.comments.all()
        comment_form = CommentForm()
        if ReviewedProducts.objects.first() is None:
            last = 'none'
        else:
            last = ReviewedProducts.objects.first().product.name
        if product.name != last:
            ReviewedProducts.objects.create(product=product, url=product.get_absolute_url())
        return render(request, 'product/detail.html', {'product': product,
                                                       'cart_product_form': cart_product_form,
                                                       'comments': comments, 'comment_form': comment_form})

    def post(self,request,id,slug):
        product = get_object_or_404(Product, id=id, slug=slug)
        cart_product_form = CartAddProductForm()
        comments = product.comments.all()
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.product = product
            new_comment.user = request.user
            new_comment.save()
        return render(request, 'product/detail.html', {'product': product,
                                                       'cart_product_form': cart_product_form,
                                                       'comments': comments, 'comment_form': comment_form})


class RegisterFormView(FormView):
    form_class = UserCreateForm
    success_url = '/login/'
    template_name = 'product/register.html'

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)

    def form_invalid(self, form):
        return super(RegisterFormView,self).form_invalid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = 'product/login.html'
    success_url = '/'

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class SearchResultsView(ListView):
    model = Product
    template_name = 'product/search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Product.objects.filter(Q(name__icontains=query))
        return object_list


@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product,
                 quantity=cd['quantity'],
                 update_quantity=cd['update'])
    return redirect('MyShop:cart_detail')


def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('MyShop:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    return render(request, 'product/cart_detail.html', {'cart': cart})


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            cart.clear()
            return render(request,'product/created.html', {'order': order})
    else:
        form = OrderCreateForm
    return render(request, 'product/create.html', {'cart': cart, 'form': form})



