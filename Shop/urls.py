from django.urls import path,re_path

from . import views
app_name = 'MyShop'
urlpatterns = [
    path('', views.ProductView.as_view(), name='product_list'),
    path('cart/', views.cart_detail, name='cart_detail'),
    path('cart/add/<int:product_id>/', views.cart_add, name='cart_add'),
    path('cart/remove<int:product_id>/', views.cart_remove, name='cart_remove'),
    re_path(r'.*register/$', views.RegisterFormView.as_view(), name='register'),
    re_path(r'.*login/$', views.LoginFormView.as_view(), name='login'),
    re_path(r'.*logout/$', views.LogoutView.as_view(), name='logout'),
    re_path(r'.*search/$', views.SearchResultsView.as_view(), name='search_results'),
    path('create/', views.order_create, name='order_create'),
    path('<slug:category_slug>/', views.ProductView.as_view(), name='product_list_by_category'),
    path('<int:id>/<slug:slug>', views.ProductDetail.as_view(), name='product_detail'),



]
